package smartcard

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/encoding/charmap"
)

func swapHex(hexs []byte) {
	for i, num := range hexs {
		num = ((num << 4) & 0xf0) | ((num >> 4) & 0x0f)
		hexs[i] = num
	}
}
func integersToString(integers []int) string {
	result := make([]string, len(integers))

	for i, number := range integers {
		result[i] = strconv.Itoa(number)
	}

	return strings.Join(result, "")
}

func randomString(size int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	source := make([]int, size)

	for i := 0; i < size; i++ {
		source[i] = rand.Intn(9)
	}

	return integersToString(source)
}

// WriteBlockToFile i.e: write jpeg to file
func WriteBlockToFile(databytes []byte, fullname string) (int, error) {
	f, err := os.Create(fullname)
	if err != nil {
		return -1, err
	}

	defer f.Close()

	n, err := f.Write(databytes)
	//fmt.Printf("wrote %d bytes\n", n)
	return n, err
}

// WriteIDCardInfoToFile write text to file
func WriteIDCardInfoToFile(data, fullname string) error {
	f, err := os.Create(fullname)
	if err != nil {
		return err
	}

	defer f.Close()

	// n, err := f.Write(data)
	_, err = fmt.Fprintf(f, data)
	//fmt.Printf("wrote %d bytes\n", n)
	return err
}

// Windows874ToUTF8 ...
func Windows874ToUTF8(str []byte) ([]byte, error) {
	b := str
	dec := charmap.Windows874.NewDecoder()
	// Take more space just in case some characters need
	// more bytes in UTF-8 than in Windows874.
	bUTF := make([]byte, len(b)*3)
	n, _, err := dec.Transform(bUTF, b, false)
	if err != nil {

	}
	bUTF = bUTF[:n]

	return bUTF, err
}

// TestDataThai ...
func TestDataThai(str []byte) []byte {
	temp, _ := Windows874ToUTF8(str)
	fmt.Printf("UTF8 is %s\r\n", temp)
	return temp
}

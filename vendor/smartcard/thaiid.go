package smartcard

import (
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/ebfe/scard"
)

// ThaiIDCardData ...
type ThaiIDCardData struct {
	Error          string `json:"error"`
	CardVersion    string `json:"version"`
	CardType       string `json:"card_type"`
	IDcardNo       string `json:"card_no"`
	FullNameTH     string `json:"full_name_th"`
	FullNameEN     string `json:"full_name_en"`
	CardIssueDate  string `json:"card_issue_date"`
	CardExpireDate string `json:"card_expire_date"`
	Birthdate      string `json:"birth_date"`
	Gender         string `json:"gender"`
	PreAddress     string `json:"preaddress"`
	Address        string `json:"address"`
	CardImgBase64  string `json:"card_img"`
	OldBP1No       string `json:"old_bp1no"`
	BP1No          string `json:"bp1no"`
	ChipNo         string `json:"chipno"`
	XXX            string `json:"xxx"`
	NationalHealth string `json:"national_health"`
	Laser          string `json:"laser"`
}

func convertAllToUTF8(idcard *ThaiIDCardData) error {
	temp, _ := Windows874ToUTF8([]byte(idcard.IDcardNo))
	idcard.IDcardNo = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.FullNameTH))
	idcard.FullNameTH = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.FullNameEN))
	idcard.FullNameEN = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.CardIssueDate))
	idcard.CardIssueDate = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.CardExpireDate))
	idcard.CardExpireDate = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.Birthdate))
	idcard.Birthdate = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.Gender))
	idcard.Gender = string(temp)
	temp, _ = Windows874ToUTF8([]byte(idcard.Address))
	idcard.Address = string(temp)
	return nil
}
func standardizeSpaces(s string) string {
	return strings.Join(strings.Fields(s), " ")
}

// TestReadNewApplet ...
func TestReadNewApplet() error {
	// Establish a PC/SC context
	context, err := scard.EstablishContext()
	if err != nil {
		fmt.Println("Can not EstablishContext:", err)
		return errors.New("Can not EstablishContext")
	}

	// Release the PC/SC context (when needed)
	defer context.Release()

	// List available readers
	readers, err := context.ListReaders()
	if err != nil {
		fmt.Println("Reader not found:", err)
		return errors.New("Reader not found")
	}
	fmt.Printf("Found %d readers:\n", len(readers))
	for i, reader := range readers {
		fmt.Printf("[%d] %s\n", i, reader)
	}
	if len(readers) > 0 {
		var err error
		var selectRsp []byte
		var index int
		// var card
		// var cardStatus

		fmt.Println("Waiting for a Card")
		index, err = WaitUntilCardPresent(context, readers, 3*time.Second)
		if err != nil {
			fmt.Println("Error CardPresent:", err)
			return errors.New("Card not insert")
		}
		index = 0
		// Connect to card
		fmt.Println("Connecting to card in ", readers[index])
		card, err := context.Connect(readers[index], scard.ShareExclusive, scard.ProtocolAny)
		if err != nil {
			fmt.Println("Error Connect:", err)
			return errors.New("Can not Connect card")
		}
		defer card.Disconnect(scard.LeaveCard)

		// getATR
		cardStatus, err := card.Status()
		if err != nil {
			fmt.Println("Error Status:", err)
			return errors.New("Can not get card Status")
		}
		fmt.Printf("ATR : % 02X\r\n", cardStatus.Atr)
		if cardStatus.Atr[0] == 0x3B && cardStatus.Atr[1] == 0x67 {
			/* corruption card */
			fmt.Println("corruption card")
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_01();
		} else {
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_02();
		}
		// APDUThaiIDCardSelect[0][12] = byte(0x83)
		var APDUThaiIDCardSelectX = [][]byte{
			{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x84, 0x06, 0x00, 0x02},
			{0x00, 0xc0, 0x00, 0x00, 0x0a},
		}
		selectRsp, err = APDUGetRsp(card, APDUThaiIDCardSelectX)
		fmt.Printf("APDU %x\r\n", APDUThaiIDCardSelectX)
		fmt.Printf("RESP % 02X\r\n", selectRsp)

		var APDUThaiIDCardX = [][]byte{
			{0x80, 0xb0, 0x00, 0x00, 0x02, 0x00, 0xff},
			{0x00, 0xc0, 0x00, 0x00, 0xff},
		}
		for i := 0; i < 0xff; i++ {
			APDUThaiIDCardX[0][2] = byte(i)
			fmt.Printf("APDU %x\r\n", APDUThaiIDCardX)
			cid, err := APDUGetRsp(card, APDUThaiIDCardX)
			if err != nil {
				fmt.Println("Error APDUGetRsp: ", err)
			}
			cid = bytes.Trim(cid, "\x00")
			fmt.Printf("cid: % 02X\n", cid)
			fmt.Printf("cid: %s\n", cid)
		}

	}
	return nil
}

// BroutForceSelectThaiID ...
func BroutForceSelectThaiID() error {
	// Establish a PC/SC context
	context, err := scard.EstablishContext()
	if err != nil {
		fmt.Println("Can not EstablishContext:", err)
		return errors.New("Can not EstablishContext")
	}

	// Release the PC/SC context (when needed)
	defer context.Release()

	// List available readers
	readers, err := context.ListReaders()
	if err != nil {
		fmt.Println("Reader not found:", err)
		return errors.New("Reader not found")
	}
	fmt.Printf("Found %d readers:\n", len(readers))
	for i, reader := range readers {
		fmt.Printf("[%d] %s\n", i, reader)
	}
	if len(readers) > 0 {
		var err error
		var selectRsp []byte
		var index int
		// var card
		// var cardStatus

		fmt.Println("Waiting for a Card")
		index, err = WaitUntilCardPresent(context, readers, 3*time.Second)
		if err != nil {
			fmt.Println("Error CardPresent:", err)
			return errors.New("Card not insert")
		}
		index = 0
		// Connect to card
		fmt.Println("Connecting to card in ", readers[index])
		card, err := context.Connect(readers[index], scard.ShareExclusive, scard.ProtocolAny)
		if err != nil {
			fmt.Println("Error Connect:", err)
			return errors.New("Can not Connect card")
		}
		defer card.Disconnect(scard.LeaveCard)

		// getATR
		cardStatus, err := card.Status()
		if err != nil {
			fmt.Println("Error Status:", err)
			return errors.New("Can not get card Status")
		}
		fmt.Printf("ATR : % 02X\r\n", cardStatus.Atr)
		if cardStatus.Atr[0] == 0x3B && cardStatus.Atr[1] == 0x67 {
			/* corruption card */
			fmt.Println("corruption card")
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_01();
		} else {
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_02();
		}
		// APDUThaiIDCardSelect[0][12] = byte(0x83)
		// var APDUThaiIDCardSelectX = [][]byte{
		// 	{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x84, 0x06, 0x00, 0x02},
		// 	{0x00, 0xc0, 0x00, 0x00, 0x0a},
		// }
		// selectRsp, err = APDUGetRsp(card, APDUThaiIDCardSelectX)
		// fmt.Printf("APDU %x\r\n", APDUThaiIDCardSelectX)
		// fmt.Printf("RESP % 02X\r\n", selectRsp)

		// selectRsp, err = APDUGetRsp(card, APDUThaiIDCardSelect)
		// fmt.Printf("APDU %x\r\n", APDUThaiIDCardSelect)
		// if err != nil {
		// 	fmt.Printf("Error Transmit: %s\r\n", err)
		// }
		// fmt.Printf("resp APDUThaiIDCardSelect: %x\r\n", selectRsp)

		// GetResponse[0][4] = byte(0x0a)
		// selectRsp, err = APDUGetRsp(card, GetResponse)
		// fmt.Printf("APDU %x\r\n", GetResponse)
		// if err != nil {
		// 	fmt.Printf("Error Transmit: %s\r\n", err)
		// }
		// fmt.Printf("resp GetResponse: %x\r\n", selectRsp)

		// APDUThaiIDCardCIDTest := [][]byte{
		// 	{0x80, 0xb0, 0x02, 0x00, 0x02, 0x00, 0xff}, // APDUThaiIDCardCID
		// 	{0x00, 0xc0, 0x00, 0x00, 0xff},             // APDUThaiIDCardCIDRsp
		// }
		// CID, err := APDUGetRsp(card, APDUThaiIDCardCIDTest)
		// fmt.Printf("APDU %x\r\n", APDUThaiIDCardCIDTest)
		// if err != nil {
		// 	fmt.Printf("Error Transmit: %s\r\n", err)
		// }
		// fmt.Printf("resp APDUThaiIDCardCIDTest: % 02x\r\n", CID)
		// APDUThaiIDCardCIDTest = [][]byte{
		// 	{0x80, 0xb0, 0x03, 0x00, 0x02, 0x00, 0xff}, // APDUThaiIDCardCID
		// 	{0x00, 0xc0, 0x00, 0x00, 0xff},             // APDUThaiIDCardCIDRsp
		// }
		// CID, err = APDUGetRsp(card, APDUThaiIDCardCIDTest)
		// fmt.Printf("APDU %x\r\n", APDUThaiIDCardCIDTest)
		// if err != nil {
		// 	fmt.Printf("Error Transmit: %s\r\n", err)
		// }
		// fmt.Printf("resp APDUThaiIDCardCIDTest: % 02x\r\n", CID)

		breakFlg := false

		// [00a4040008a0000000 46 4c c0 ff]
		//46
		for l := 0x84; l <= 0xff; l++ { // A-Z
			APDUThaiIDCardSelect[0][9] = byte(l)
			for k := 0x00; k <= 0xff; k++ {
				APDUThaiIDCardSelect[0][10] = byte(k)
				for j := 0x00; j <= 0x00; j++ {
					APDUThaiIDCardSelect[0][11] = byte(j)
					APDUThaiIDCardSelect[0][12] = 0x00
					fmt.Printf("APDU % 02x\r\n", APDUThaiIDCardSelect)
					for i := 0x00; i <= 0x02; i++ {
						APDUThaiIDCardSelect[0][12] = byte(i)
						//																									  l		k	   j 	i
						if bytes.Equal(APDUThaiIDCardSelect[0], []byte{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01}) ||
							bytes.Equal(APDUThaiIDCardSelect[0], []byte{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x05}) ||
							bytes.Equal(APDUThaiIDCardSelect[0], []byte{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x83}) ||
							bytes.Equal(APDUThaiIDCardSelect[0], []byte{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x84, 0x06, 0x00, 0x00}) ||
							bytes.Equal(APDUThaiIDCardSelect[0], []byte{0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x00, 0x84, 0x06, 0x00, 0x02}) {
							fmt.Printf("APDU %x\r\n", APDUThaiIDCardSelect)
							continue
						}
						// Send select APDU
						selectRsp, err = APDUGetRsp(card, APDUThaiIDCardSelect)
						if err != nil {
							fmt.Printf("Error Transmit: %s\r\n", err)
						} else {
							if selectRsp[0] == byte(0x61) {
								fmt.Printf("APDU %x\r\n", APDUThaiIDCardSelect)
								fmt.Printf("resp APDUThaiIDCardSelect: %x\r\n", selectRsp)
								breakFlg = true
								break
							}
						}
					}
					if breakFlg {
						break
					}
				}
				if breakFlg {
					break
				}
			} // end for k
			if breakFlg {
				break
			}
		} // end for l
	}
	return nil
}

// ReadThaiIDCardData ...
func ReadThaiIDCardData() (ThaiIDCardData, error) {
	var idcard ThaiIDCardData
	// Establish a PC/SC context
	context, err := scard.EstablishContext()
	if err != nil {
		fmt.Println("Can not EstablishContext:", err)
		return idcard, errors.New("Can not EstablishContext")
	}

	// Release the PC/SC context (when needed)
	defer context.Release()

	// List available readers
	readers, err := context.ListReaders()
	if err != nil {
		fmt.Println("Reader not found:", err)
		return idcard, errors.New("Reader not found")
	}
	fmt.Printf("Found %d readers:\n", len(readers))
	for i, reader := range readers {
		fmt.Printf("[%d] %s\n", i, reader)
	}
	if len(readers) > 0 {
		fmt.Println("Waiting for a Card")
		index, err := WaitUntilCardPresent(context, readers, 3*time.Second)
		if err != nil {
			fmt.Println("Error CardPresent:", err)
			return idcard, errors.New("Card not insert")
		}
		index = 0
		// Connect to card
		fmt.Println("Connecting to card in ", readers[index])
		card, err := context.Connect(readers[index], scard.ShareExclusive, scard.ProtocolAny)
		if err != nil {
			fmt.Println("Error Connect:", err)
			return idcard, errors.New("Can not Connect card")
		}
		defer card.Disconnect(scard.LeaveCard)
		// getATR
		cardStatus, err := card.Status()
		if err != nil {
			fmt.Println("Error Status:", err)
			return idcard, errors.New("Can not get card Status")
		}
		fmt.Printf("ATR : % 02X\r\n", cardStatus.Atr)
		if cardStatus.Atr[0] == 0x3B && cardStatus.Atr[1] == 0x67 {
			/* corruption card */
			fmt.Println("corruption card")
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_01();
		} else {
			// _apdu = new APDU_THAILAND_IDCARD_TYPE_02();
		}

		// // select MOI Applet
		// Send select APDU
		selectRsp, err := APDUGetRsp(card, APDUThaiIDCardSelect)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			goto end
		}
		fmt.Println("resp APDUThaiIDCardSelect: ", selectRsp)
		if selectRsp[0] != byte(0x61) {
			fmt.Println("SmartCard not support(Cannot select Ministry of Interior Applet.)")
			goto end
		}

		cardVersion, err := APDUGetRsp(card, APDUThaiIDCardVersion)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		cardVersion = bytes.Trim(cardVersion, "\x00")

		idcard.CardVersion = string(cardVersion)
		fmt.Printf("Version : %s\n", idcard.CardVersion)

		cid, err := APDUGetRsp(card, APDUThaiIDCardCID)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		cid = bytes.Trim(cid, "\x00")
		idcard.IDcardNo = string(cid)
		// fmt.Printf("cid: %s\n", idcard.IDcardNo)

		fullnameEN, err := APDUGetRsp(card, APDUThaiIDCardFullnameEn)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		fullnameEN = bytes.Trim(fullnameEN, "\x00")
		fullnameEN = bytes.TrimSpace(fullnameEN)
		fullnameEN = bytes.Replace(fullnameEN, []byte("#"), []byte(" "), -1)
		idcard.FullNameEN = standardizeSpaces(string(fullnameEN))
		// fmt.Printf("fullnameEN: %s\n", idcard.FullNameEN)

		fullnameTH, err := APDUGetRsp(card, APDUThaiIDCardFullnameTh)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		fullnameTH = bytes.Trim(fullnameTH, "\x00")
		fullnameTH = bytes.TrimSpace(fullnameTH)
		fullnameTH = bytes.Replace(fullnameTH, []byte("#"), []byte(" "), -1)
		idcard.FullNameTH = standardizeSpaces(string(fullnameTH))
		// fmt.Printf("fullnameTH: %s\n", idcard.FullNameTH)

		birth, err := APDUGetRsp(card, APDUThaiIDCardBirth)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		birth = bytes.Trim(birth, "\x00")
		idcard.Birthdate = string(birth)
		// fmt.Printf("birth: %s\n", idcard.Birthdate)

		gender, err := APDUGetRsp(card, APDUThaiIDCardGender)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		gender = bytes.Trim(gender, "\x00")
		if string(gender) == "1" {
			idcard.Gender = "male" // 1 = Male, 2 = Female
		} else if string(gender) == "2" {
			idcard.Gender = "female" // 1 = Male, 2 = Female
		}
		// fmt.Printf("gender: %s\n", idcard.Gender)

		oldBP1No, err := APDUGetRsp(card, APDUThaiIDCardOldBP1No)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		oldBP1No = bytes.Trim(oldBP1No, "\x00")
		idcard.OldBP1No = string(oldBP1No)

		issuer, err := APDUGetRsp(card, APDUThaiIDCardIssuer)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		issuer = bytes.Trim(issuer, "\x00")
		// idcard.CardIssuer = string(issuer)
		// fmt.Printf("issuer: %s\n", string(issuer))

		issueDate, err := APDUGetRsp(card, APDUThaiIDCardIssuedate)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		issueDate = bytes.Trim(issueDate, "\x00")
		idcard.CardIssueDate = string(issueDate)
		// fmt.Printf("issueDate: %s\n", idcard.CardIssueDate)

		cardExp, err := APDUGetRsp(card, APDUThaiIDCardExpiredate)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		cardExp = bytes.Trim(cardExp, "\x00")
		idcard.CardExpireDate = string(cardExp)
		// fmt.Printf("cardExp: %s\n", idcard.CardExpireDate)

		preAddress, err := APDUGetRsp(card, APDUThaiIDCardPreAddress)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		preAddress = bytes.Trim(preAddress, "\x00")
		idcard.PreAddress = string(preAddress)

		address, err := APDUGetRsp(card, APDUThaiIDCardAddress)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		address = bytes.Trim(address, "\x00")
		address = bytes.TrimSpace(address)
		address = bytes.Replace(address, []byte("#"), []byte(" "), -1)
		idcard.Address = standardizeSpaces(string(address))

		postAddress, err := APDUGetRsp(card, APDUThaiIDCardPostAddress)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		postAddress = bytes.Trim(postAddress, "\x00")
		// idcard.PreAddress = string(postAddress)
		fmt.Printf("post address: %s\n", string(postAddress))

		// XXX, err := APDUGetRsp(card, APDUXXX)
		// if err != nil {
		// 	fmt.Println("Error APDUGetRsp: ", err)
		// 	goto end
		// }
		// XXX = bytes.Trim(XXX, "\x00")
		// idcard.XXX = string(XXX)

		cardPhotoJpg, err := APDUGetBlockRsp(card, APDUThaiIDCardPhoto, APDUThaiIDCardPhotoRsp)
		if err != nil {
			fmt.Println("Error: ", err)
			goto end
		}
		// t := time.Now()
		// fullnameENFilenameExt := fmt.Sprintf("./images/idcard/%s_%s.jpg", t.Format("20060102_150405"), strings.Replace(string(fullnameEN), "#", "_", -1))
		fullnameENFilenameExt := fmt.Sprintf("./images/idcard/%s_%s.jpg", idcard.IDcardNo, strings.Replace(idcard.FullNameEN, " ", "_", -1))
		fmt.Printf("Image Save at: %s\n", fullnameENFilenameExt)
		n2, err := WriteBlockToFile(cardPhotoJpg, fullnameENFilenameExt)
		if err != nil {
			fmt.Println("Error WriteBlockToFile: ", err)
			goto end
		}
		fmt.Printf("wrote %d bytes\n", n2)

		bp1no, err := APDUGetRsp(card, APDUThaiIDCardBP1No)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		bp1no = bytes.Trim(bp1no, "\x00")
		idcard.BP1No = string(bp1no)

		// End MOI Applet
		////////////////////////////////////////////////////

		////////////////////////////////////////////////////
		// Send select Laser Applet
		selectRsp, err = APDUGetRsp(card, APDUThaiIDCardLaserAppletSelect)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			goto end
		}
		fmt.Println("resp APDUThaiIDCardLaserAppletSelect: ", selectRsp)
		if selectRsp[0] != byte(0x61) {
			fmt.Println("SmartCard not support(Cannot select Laser Applet.)")
			goto end
		}
		chipno, err := APDUGetRsp(card, APDUThaiIDCardChipNo)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		fmt.Printf("ChipNo : % 02X\r\n", chipno)
		// 9F 7F 2A 40 90 61 64 40 91 90 37 2A 21 60 07 26 04 3A 19 3B 19 40 90 92 85 40 91 92 85 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
		// 9F 7F 2A 40 90 72 27 19 81 51 78 01 00 70 12 FE 04 58 42 26 9D 12 92 70 12 12 93 70 12 12 94 70 12 00 00 00 36 00 00 00 00 00 00 00 00 00 00
		chipno = bytes.Trim(chipno, "\x00")
		// chipnoArr := bytes.Split(chipno, []byte{0x00})
		idcard.ChipNo = hex.EncodeToString(chipno[13:21])

		selectRsp, err = APDUGetRsp(card, APDUThaiIDCardLaserAppletSelect)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			goto end
		}
		fmt.Println("resp APDUThaiIDCardLaserAppletSelect: ", selectRsp)
		if selectRsp[0] != byte(0x61) {
			fmt.Println("SmartCard not support(Cannot select Laser Applet.)")
			goto end
		}
		laser, err := APDUGetRsp(card, APDUThaiIDCardLaser)
		if err != nil {
			fmt.Println("Error APDUGetRsp: ", err)
			goto end
		}
		laser = bytes.Trim(laser, "\x00")
		laserArr := bytes.Split(laser, []byte{0x10})
		idcard.Laser = string(laserArr[1]) // ME0114690983

	}
end:
	convertAllToUTF8(&idcard)
	return idcard, err
}

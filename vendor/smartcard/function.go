package smartcard

import (
	"fmt"
	"time"

	"github.com/ebfe/scard"
)

// WaitUntilCardPresent ...
func WaitUntilCardPresent(ctx *scard.Context, readers []string, timeout time.Duration) (int, error) {
	rs := make([]scard.ReaderState, len(readers))
	for i := range rs {
		rs[i].Reader = readers[i]
		rs[i].CurrentState = scard.StateUnaware
	}

	for {
		for i := range rs {
			// fmt.Printf("Event : 0x%X\r\n", rs[i].EventState)
			// fmt.Printf("[%d] ", i)
			// printCardState(rs[i].EventState)
			// fmt.Printf("%x %x\r\n", rs[i].EventState&scard.StatePresent, rs[i].EventState&scard.StateMute)
			if (rs[i].EventState&scard.StatePresent != 0) && (rs[i].EventState&scard.StateMute == 0) {
				return i, nil
			}
			rs[i].CurrentState = rs[i].EventState
		}
		err := ctx.GetStatusChange(rs, time.Duration(timeout))
		if err != nil {
			return -1, err
		}
	}
}

// APDUGetRsp Send list of APDU and get last command response
// ispadzeroOptional is optional(default = true) to replace adpu tail section
func APDUGetRsp(card *scard.Card, apducmds [][]byte, ispadzeroOptional ...bool) ([]byte, error) {
	var resp []byte

	ispadzero := true
	if len(ispadzeroOptional) > 0 {
		ispadzero = ispadzeroOptional[0]
	}

	// Send command APDU: apducmds
	for _, apducmd := range apducmds {
		rsp, err := card.Transmit(apducmd)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			return nil, err
		}
		//printRsp(rsp)
		resp = rsp
	}

	// pad zero
	if ispadzero == true && len(resp) > 2 {
		dlen := len(resp)
		resp[dlen-2] = 0
	}

	return resp, nil
}

// APDUGetBlockRsp Send list of APDU and append all response
func APDUGetBlockRsp(scardCard *scard.Card, apducmds [][]byte, apducmdRsp []byte) ([]byte, error) {
	var respBlock []byte
	card := scardCard

	for _, apducmd1 := range apducmds {
		// Send command APDU: apducmd1
		rsp, err := card.Transmit(apducmd1)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			return nil, err
		}
		// printRsp(rsp)

		// Send command APDU: apducmdRsp
		rsp, err = card.Transmit(apducmdRsp)
		if err != nil {
			fmt.Println("Error Transmit:", err)
			return nil, err
		}
		//printRsp(rsp)

		respBlock = append(respBlock, rsp[:len(rsp)-2]...)

	}
	// fmt.Printf("% 2X\n", respBlock)

	return respBlock, nil
}

func printRsp(rsp []byte) {
	fmt.Println("resp: ", rsp)
	for i := 0; i < len(rsp)-2; i++ {
		fmt.Printf("%c", rsp[i])
	}
	fmt.Println()
}

# iqsmartcard


## How to create MSI

Install go-msi
```
https://github.com/mh-cbon/go-msi/releases
```
Install choco
```
https://chocolatey.org/install#install-with-cmdexe

Run CMD As Admin

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

Install WIX

```
http://wixtoolset.org/releases/
```

Create MSI
```
copy wix.json.template to wix.json

go-msi set-guid

go-msi generate-templates --version 0.0.1

Copy generated file to ./templates/

go-msi make --msi iqpluscard.msi --version 0.0.2
```

```
go build -o iqpluscard.exe ./main
```
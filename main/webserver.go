package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"smartcard"
	"time"

	"github.com/gorilla/websocket"
)

var (
	addr    = flag.String("addr", "localhost:19979", "http service address")
	cmdPath string
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Maximum message size allowed from peer.
	maxMessageSize = 8192

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Time to wait before force close on connection.
	closeGracePeriod = 10 * time.Second
)

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

}
func internalError(ws *websocket.Conn, msg string, err error) {
	log.Println(msg, err)
	ws.WriteMessage(websocket.TextMessage, []byte("Internal server error."))
}

var upgrader = websocket.Upgrader{}

func serveWs(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}

	defer ws.Close()

	outr, outw, err := os.Pipe()
	if err != nil {
		internalError(ws, "stdout:", err)
		return
	}
	defer outr.Close()
	defer outw.Close()

	inr, inw, err := os.Pipe()
	if err != nil {
		internalError(ws, "stdin:", err)
		return
	}
	defer inr.Close()
	defer inw.Close()

	proc, err := os.StartProcess(cmdPath, flag.Args(), &os.ProcAttr{
		Files: []*os.File{inr, outw, outw},
	})
	if err != nil {
		internalError(ws, "start:", err)
		return
	}

	inr.Close()
	outw.Close()

	stdoutDone := make(chan struct{})
	// go pumpStdout(ws, outr, stdoutDone)
	// go ping(ws, stdoutDone)

	// pumpStdin(ws, inw)

	// Some commands will exit when stdin is closed.
	inw.Close()

	// Other commands need a bonk on the head.
	if err := proc.Signal(os.Interrupt); err != nil {
		log.Println("inter:", err)
	}

	select {
	case <-stdoutDone:
	case <-time.After(time.Second):
		// A bigger bonk on the head.
		if err := proc.Signal(os.Kill); err != nil {
			log.Println("term:", err)
		}
		<-stdoutDone
	}

	if _, err := proc.Wait(); err != nil {
		log.Println("wait:", err)
	}
}

func readIDCardHandler(w http.ResponseWriter, r *http.Request) {
	var idcard smartcard.ThaiIDCardData
	// enableCors(&w)
	//allow cross domain AJAX requests
	w.Header().Set("Access-Control-Allow-Origin", "http://iqplus.local")
	// w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
	// w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")

	fmt.Println("READ ID CARD")
	// fmt.Fprintf(w, "READ ID CARD")
	idcard, err := smartcard.ReadThaiIDCardData()
	if err != nil {
		fmt.Println("ReadThaiIDCardData : ", err.Error())
		idcard.Error = err.Error()
	}

	jsonRespBytes, err := json.MarshalIndent(idcard, "", "\t")
	fmt.Println(string(jsonRespBytes))
	jsonRespBytes, err = json.Marshal(idcard)

	fmt.Fprintf(w, string(jsonRespBytes))
}

type TestData struct {
	var1 string `json:"var1"`
	var2 string `json:"var2"`
}

func testUTF8(w http.ResponseWriter, r *http.Request) {
	var data TestData
	var test = []byte{0x6F, 0x64, 0x84, 0x08, 0xA0, 0x01, 0x51, 0xA5, 0x58, 0x73, 0x49, 0x06, 0x07, 0x2A, 0x86, 0x48, 0x86, 0xFC, 0x6B, 0x01, 0x60, 0x0C, 0x06, 0x0A, 0x2A, 0x86, 0x48, 0x86, 0xFC, 0x6B, 0x02, 0x02, 0x02, 0x01, 0x63, 0x09, 0x06, 0x07, 0x2A, 0x86, 0x48, 0x86, 0xFC, 0x6B, 0x03, 0x64, 0x0B, 0x06, 0x09, 0x2A, 0x86, 0x48, 0x86, 0xFC, 0x6B, 0x04, 0x02, 0x15, 0x65, 0x0A, 0x06, 0x08, 0x2A, 0x86, 0x48, 0x86, 0xFC, 0x6B, 0x05, 0x05, 0x66, 0x0C, 0x06, 0x0A, 0x2B, 0x06, 0x01, 0x04, 0x01, 0x2A, 0x02, 0x6E, 0x01, 0x03, 0x9F, 0x6E, 0x06, 0x19, 0x81, 0x51, 0x78, 0x01, 0x9F, 0x65, 0x01, 0xFF}
	var test1 = []byte{0x9F, 0x7F, 0x2A, 0x40, 0x90, 0x72, 0x27, 0x19, 0x81, 0x51, 0x78, 0x01, 0x70, 0x12, 0xFE, 0x04, 0x58, 0x42, 0x26, 0x9D, 0x12, 0x92, 0x70, 0x12, 0x12, 0x93, 0x70, 0x12, 0x12, 0x94, 0x70, 0x12, 0x36}
	data.var1 = string(smartcard.TestDataThai(test))
	data.var2 = string(smartcard.TestDataThai(test1))
	jsonRespBytes, _ := json.MarshalIndent(data, "", "\t")
	fmt.Println(string(jsonRespBytes))
	jsonRespBytes, _ = json.Marshal(data)

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonRespBytes))
}
func webServer() {
	http.HandleFunc("/ws", serveWs)
	http.HandleFunc("/readidcard/", readIDCardHandler)
	http.HandleFunc("/test/", testUTF8)
	fmt.Println("Listening ", *addr)
	fmt.Println("===========================================")
	fmt.Println("How to use")
	fmt.Printf("===========================================\n\n")
	fmt.Printf("Read ID card : %s/readidcard\r\n", *addr)
	fmt.Println("\n===========================================")
	log.Fatal(http.ListenAndServe(*addr, nil))
}

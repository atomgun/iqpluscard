package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"smartcard"
	"strings"
	"syscall"
	"time"

	"github.com/tiaguinho/gosoap"
)

func envInit() {
	// Create ID Card image dir
	path := filepath.Join("./images/idcard/")
	fmt.Println("Create ", path)
	os.MkdirAll(path, os.ModePerm)

	// Create webcam image dir
	path = "./webcam/"
	fmt.Println("Create ", path)
	os.MkdirAll(path, os.ModePerm)
}

// CheckDeathStatusResponse ...
type CheckDeathStatusResponse struct {
	CheckDeathStatusResult CheckDeathStatusResult
}

// CheckDeathStatusResult ...
type CheckDeathStatusResult struct {
	isError   string
	errorDesc string
	dataInfo  string
	// CountryName       string
	// CountryCode       string
}

var (
	r CheckDeathStatusResponse
)

func checkDeathStatusSoap() {
	soap, err := gosoap.SoapClient("http://idcard.bora.dopa.go.th/CheckStatus/POPStatusService.asmx?WSDL")
	if err != nil {
		fmt.Printf("error not expected: %s\r\n", err.Error())
	}

	params := gosoap.Params{
		"pid": "1160400101979",
		"dob": 25320305,
	}

	err = soap.Call("CheckDeathStatus", params)
	if err != nil {
		fmt.Printf("error in soap call: %s\r\n", err.Error())
	}

	soap.Unmarshal(&r)
	fmt.Println(r)

}
func main() {
	envInit()
	// Graceful shutdown
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGTERM, os.Interrupt)
	// signal.Notify(shutdown, syscall.SIGINT)
	go func() {
		sig := <-shutdown
		fmt.Printf("Caught sig: %+v", sig)
		fmt.Printf("\r\nShutting Down ....")
		for index := 0; index < 3; index++ {
			time.Sleep(1 * time.Second)
			fmt.Printf(" .")
		}
		os.Exit(0)
	}()

	go webServer()
	// Test read @Dev state
	idcard, _ := smartcard.ReadThaiIDCardData()
	jsonRespBytes, _ := json.MarshalIndent(idcard, "", "\t")
	fmt.Println(string(jsonRespBytes))

	// A select blocks until one of its cases can run, then it executes that case. It chooses one at random if multiple are ready.
	// select {
	// case <-shutdown:
	// 	fmt.Println("Shutting Down ....")
	// }

	// Or only wait channel have signal
	// <-shutdown
	// fmt.Printf("\r\nShutting Down ....")
	// for index := 0; index < 3; index++ {
	// 	time.Sleep(1 * time.Second)
	// 	fmt.Printf(" .")
	// }

	reader := bufio.NewReader(os.Stdin)
	for {
		input, _ := reader.ReadString('\n')
		input = strings.TrimSpace(input)
		input = strings.Trim(input, "\n")
		switch input {
		case "exit":
			shutdown <- syscall.SIGTERM
		default:

		}
	}
}
